def do_collection(m, *strings)
  result = []
  strings.each{|str| result << str.send(m)}
  return result
end

collection = %w{hello readers}

result_collection = do_collection( :upcase, *collection)
puts result_collection

result_collection = do_collection( :downcase, *result_collection)
puts result_collection

def create_point(x, y, color: "white", size: 1, **h)
  p [x, y, color, size, h]
end

create_point(2, 3,style:"solid", styler: "blue")

def multisum(a, b, *c)
  p c
  sum = c.inject{|sum, num| sum + num}
  p sum + b + a
end

multisum(5, *[1,2,3,4,5])

# Blocks


# lambda

s = lambda do |a,b| return a+b
end

puts s.call(1,2)


p = Proc.new {|a,b| a+b}

p p.call(1,4)


# Class

class Dog

  def ahrr
    puts "agrrhr"
  end

end

dog = Dog.new
dog.ahrr

$u = 12

class Point
  attr_accessor :x, :y, :i, :r

  def initialize(x,y)
    @x=x
    @y=y
    @i = $u
    @@r = 555
  end

  def distance()
    return @@r
  end
end

p = Point.new(20, 20)
p.x = 9
p p.x, p.y, p.i
p p.distance()