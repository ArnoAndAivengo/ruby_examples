class Item

    attr_reader :item_name, :qty

    def initialize(item_name, qty)
        @item_name = item_name
        @qty = qty
    end

    def to_s
        "Item (#{@item_name}, #{@qty})"
    end

    def hash
        self.item_name.hash ^ self.qty.hash
    end

    def eql?(other_item)
        # puts "Method == called on #{self} with parameter #{other_item}"
        # true
        puts "#eql? invoked"
        self.item_name == other_item.item_name && self.qty == other_item.qty
    end
        
end

# puts Item.new("abcd", 1) == Item.new("abcd", 3)

items = [Item.new("abcd", 1), Item.new("abcd", 1), Item.new("abcd", 1)]

puts items.uniq